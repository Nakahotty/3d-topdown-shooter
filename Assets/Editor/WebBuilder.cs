﻿using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEngine;

public class WebBuilder
{
    static void build()
    {
        string[] scenes = new string[SceneManager.sceneCountInBuildSettings];
		for(int i = 0; i < scenes.Length; i++)
        {
            scenes[i] = SceneUtility.GetScenePathByBuildIndex(i);
        }
        BuildPipeline.BuildPlayer(scenes, "webBuild", BuildTarget.WebGL, BuildOptions.None);
    }
}
