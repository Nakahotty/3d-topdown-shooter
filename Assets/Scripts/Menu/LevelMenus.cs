﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelMenus : MonoBehaviour
{
    [SerializeField] private GameObject gameOverPanel;
    [SerializeField] private GameObject pausePanel;
    [SerializeField] private Text scoreLabel;

    public void Pause() {
        pausePanel.SetActive(true);
        Time.timeScale = 0f;
    }

    public void Unpause() {
        pausePanel.SetActive(false);
        Time.timeScale = 1f;
    }

    public void ShowGameOver() {
        gameOverPanel.SetActive(true);
        scoreLabel.text = "Your score is " + ScoreManager.Instance.ScoreValue;
        Invoke("StopTimeOnDeath", 3f);
    }

    public void Quit() {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menu");
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (Time.timeScale == 0f) {
                Unpause();
            }
            else {
                Pause();
            }
        }
    }

    private void StopTimeOnDeath()
    {
        Time.timeScale = 0;
    }
}
