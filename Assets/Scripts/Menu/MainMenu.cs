﻿/// <summary>
/// This class will be on the play button in the menu
/// and will start the game after button got pressed
/// </summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class MainMenu : MonoBehaviour {
    [SerializeField] private Texture2D cursorTexture;
    [SerializeField] private Text highscoreLabel;

    public void Start() {
        Application.targetFrameRate = 60;
        if (cursorTexture) {
            Cursor.SetCursor(cursorTexture, new Vector2(cursorTexture.width / 2, cursorTexture.height / 2), CursorMode.Auto);
        }

        if (highscoreLabel) {
            highscoreLabel.text = "Highscore " + PlayerPrefs.GetInt("highscore");
        }
    }

    public void Play() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void Exit() {
        Debug.Log("EXIT!");
        Application.Quit();
    }

    public void PlayClickSound()
    {
        AudioManager.Instance.PlaySound("ClickSound");
    }
}
