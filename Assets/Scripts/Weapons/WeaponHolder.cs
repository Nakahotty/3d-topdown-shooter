﻿/// <summary>
/// This class will be attached to the player and 
/// will tell him what weapon he holds and how it shoots
/// </summary>
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class WeaponHolder : MonoBehaviour {
    private UnityAction<int> onAmmoChange;
    [SerializeField] private Animator animator;
    [SerializeField] private GameObject basePickupPrefab;
    [SerializeField] private GameObject meleeHitbox;

    private float fireTimeout;
    private float cooldown = 0;
    [SerializeField] private Weapon currentlyHoldedWeapon;

    public Weapon GetCurrentlyHoldedWeapon() {
        return currentlyHoldedWeapon;
    }

    public void SubscribeToAmmoChange(UnityAction<int> func) {
        onAmmoChange += func;
    }

    public void UnsubscribeToAmmoChange(UnityAction<int> func) {
        onAmmoChange -= func;
    }

    public void PickUpWeapon(Weapon weapon) {
        DropWeapon();
        currentlyHoldedWeapon = Instantiate(weapon);
        fireTimeout = 1 / weapon.Firerate;
        animator.runtimeAnimatorController = currentlyHoldedWeapon.runtimeAnimator;

        if (onAmmoChange != null) {
            onAmmoChange(currentlyHoldedWeapon.ammo);
        }
        
        if (currentlyHoldedWeapon.IsMeele) {
            meleeHitbox.GetComponent<DamageDealer>().SetDamageDealer(currentlyHoldedWeapon.Damage, currentlyHoldedWeapon.Force);
        }
    }

    public void DropWeapon() {
        if (currentlyHoldedWeapon == null) {
            return;
        }

        // No pickup sprite means that the weapon is not intended for throwing.
        if (currentlyHoldedWeapon.pickupSprite) {
            GameObject pickupGO = Instantiate(basePickupPrefab, transform.position, Quaternion.identity);
            Pickup pickup = pickupGO.GetComponent<Pickup>();
            pickup.SetWeapon(currentlyHoldedWeapon);
            pickup.Throw(transform.eulerAngles.z);
        }
    }

    public void Shoot() {
        if (cooldown > 0 || currentlyHoldedWeapon.ammo == 0) {
            return;
        }
        
        animator.SetTrigger("Shoot");
        currentlyHoldedWeapon.ammo--;
        if (onAmmoChange != null) {
            onAmmoChange(currentlyHoldedWeapon.ammo);
        }

        cooldown = fireTimeout;
        foreach (Projectile projectile in currentlyHoldedWeapon.shootingPattern) {
            Vector3 newPos = transform.position + (projectile.firePoint.x * transform.right) + (projectile.firePoint.y * transform.up);
            GameObject projectileGO = Instantiate(projectile.prefab, newPos, Quaternion.Euler(transform.eulerAngles + new Vector3(0, 0, projectile.AngleOffset + Random.Range(-projectile.inaccuracy, projectile.inaccuracy))));
            projectileGO.GetComponent<RangedDamageDealer>().SetDamageDealer(currentlyHoldedWeapon.Damage, currentlyHoldedWeapon.Force);
        }

        if (AudioManager.Instance != null)
        {
            AudioManager.Instance.PlaySound(currentlyHoldedWeapon.SoundName);
        }
    }

    private void Update() {
        cooldown -= Time.deltaTime;
        if (Input.GetMouseButton(0)) {
            Shoot();
        }
    }

    private void Start() {
        if (currentlyHoldedWeapon != null) {
            PickUpWeapon(currentlyHoldedWeapon);
        }
    }
}
