﻿/// <summary>
/// This class will handle picking up the nearest weapon to the Player
/// </summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(WeaponHolder))]
public class WeaponCollector : MonoBehaviour {
    private WeaponHolder weaponHolder;
    [SerializeField] private List<GameObject> pickupsInRange = new List<GameObject>();

    public void CollectWeapon() {
        if (pickupsInRange.Count > 0) {
            GameObject nearestWeapon = GetNearestWeapon();
            PassWeaponToWeaponHolder(nearestWeapon);
        }
    }

    private void Start() {
        weaponHolder = GetComponent<WeaponHolder>();
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.CompareTag("Weapon")) {
            pickupsInRange.Add(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.CompareTag("Weapon")) {
            pickupsInRange.Remove(collision.gameObject);
        }
    }

    private void Update() {
        if (pickupsInRange.Count > 0 && Input.GetKeyDown("e")) {
            CollectWeapon();
        }
    }

    private GameObject GetNearestWeapon() {
        float minDistance = 10000;
        GameObject nearestWeapon = null;

        foreach (GameObject pickupGameObject in pickupsInRange) {
            if (pickupGameObject != null) {
                float currentDistance = (transform.position - pickupGameObject.transform.position).sqrMagnitude;

                if (minDistance > currentDistance) {
                    minDistance = currentDistance;
                    nearestWeapon = pickupGameObject;
                }
            }
        }

        return nearestWeapon;
    }

    private void PassWeaponToWeaponHolder(GameObject nearestWeapon) {
        if (nearestWeapon != null) {
            Weapon pickedWeapon;
            Pickup pickup = nearestWeapon.GetComponent<Pickup>();

            if (pickup != null) {
                pickedWeapon = pickup.Collect();
                pickupsInRange.Remove(nearestWeapon);
                weaponHolder.PickUpWeapon(pickedWeapon);
            }
        }
    }
}
