﻿/// <summary>
/// This class will be in every Weapon pickup GameObject.
/// </summary>
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class Pickup : MonoBehaviour {
    [SerializeField] private Weapon weapon;

    public Weapon Collect() {
        Destroy(gameObject, 0.1f);
        PlayCollectSound();
        return Instantiate(weapon);
    }

    public void SetWeapon(Weapon weapon) {
        this.weapon = weapon;
        UpdateSprite();
    }

    public Weapon GetWeapon() {
        return weapon;
    }

    public void UpdateSprite() {
        if (weapon) {
            GetComponent<SpriteRenderer>().sprite = weapon.pickupSprite;
            GetComponent<BoxCollider2D>().size = weapon.pickupSprite.bounds.size;
        }
    }

    public void Throw(float eulerZ) {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.AddForce(new Vector2(Mathf.Cos(Mathf.Deg2Rad * eulerZ), Mathf.Sin(Mathf.Deg2Rad * eulerZ)) * 5, ForceMode2D.Impulse);
        rb.angularVelocity = Random.Range(-90f, 90f);
    }

    private void OnValidate() {
        UpdateSprite();
    }

    private void PlayCollectSound()
    {
        if (AudioManager.Instance != null)
        {
            if (weapon.name != "Hammer")
            {
                AudioManager.Instance.PlaySound("PickupSound");
            }
            else
            {
                AudioManager.Instance.PlaySound("HammerSound");
            }
        }
    }
}