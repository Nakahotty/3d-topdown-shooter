﻿/// <summary>
/// This script will handle the to inflict melee damage on 
/// certain enemy by calling the TakeDamage() method from the EnemyHealth script. 
/// </summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageDealer : MonoBehaviour {
    protected int Damage { get; set; } 
    protected float Force { get; set; }

    public void SetDamageDealer(int damage, float force) {
       this.Damage = damage;
       this.Force = force;
    }
    
    protected virtual void OnTriggerEnter2D(Collider2D collision) {
        if (collision.CompareTag("EnemyHitbox")) {
            EnemyHealth enemyHealth = collision.GetComponentInParent<EnemyHealth>();
            if (enemyHealth != null) {
                enemyHealth.TakeDamage(Damage, Force, transform.position);
            }
        }
    }
}
