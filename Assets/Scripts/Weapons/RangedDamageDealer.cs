﻿/// <summary>
/// This class derives from DamageDealer and expands it for the bullets.
/// There is a game mechanic implemented here which makes the bullets 
/// lose force and damage overtime, once they have been shot.
/// </summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class RangedDamageDealer : DamageDealer {
    private const float ProjectileLifetime = 4f;
    private const int DamageDecreaserOvertime = 1;
    private const float ForceDecreaserOvertime = 1f;
    [SerializeField, Range(1000, 3000)] private float launchSpeed = 1500f;

    protected override void OnTriggerEnter2D(Collider2D collision) {
        base.OnTriggerEnter2D(collision);
        Destroy(gameObject);
    }

    private void Start() {
        GetComponent<Rigidbody2D>().AddForce(transform.right * launchSpeed, ForceMode2D.Force);
        Destroy(gameObject, ProjectileLifetime);
        InvokeRepeating("DecreaseDamageOvertime", 0, 0.1f);
        InvokeRepeating("DecreaseForceOvertime", 0, 0.1f);
    }

    private void DecreaseDamageOvertime() {
        Damage -= DamageDecreaserOvertime;
        if (Damage <= 0) {
            Damage = 0;
            CancelInvoke("DecreaseDamageOvertime");
        }
    }

    private void DecreaseForceOvertime() {
        Force -= ForceDecreaserOvertime;
        if (Force <= 0) {
            Force = 0;
            CancelInvoke("DecreaseForceOvertime");
        }
    }
}
