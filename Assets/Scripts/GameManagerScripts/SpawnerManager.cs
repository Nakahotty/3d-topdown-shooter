﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerManager : MonoBehaviour
{
    private List<GameObject> spawners = new List<GameObject>();
    [SerializeField]
    private Difficulty gameDifficulty = Difficulty.Easy;
    private float playTime = 0f;
    private uint level = 1;
    private int increaseLevelThreshold = 20; // Increase level every x seconds.
    public enum Difficulty {
        Easy, // 20 seconds
        Medium, // 15 seconds
        Hard // 12 seconds
    }

    private void Start() {
        spawners.AddRange(GameObject.FindGameObjectsWithTag("Spawner"));
        if (gameDifficulty == Difficulty.Easy) {
            increaseLevelThreshold = 20;
        } 

        if (gameDifficulty == Difficulty.Medium) {
            increaseLevelThreshold = 15;
        }

        if (gameDifficulty == Difficulty.Hard) {
            increaseLevelThreshold = 12;
        }
    }

    private void Update() {
        playTime += Time.deltaTime;
        if (playTime >= increaseLevelThreshold * level) {
            IncreaseDifficulty();
        }
    }

    private void IncreaseDifficulty() {
        level++;
        foreach (var spawner in spawners) {
            ObjectSpawnerController controller = spawner.GetComponent<ObjectSpawnerController>();
            if (controller.GetSpawnableList()[0].tag == "Weapon" && controller.GetSpawnTime() < 20) {
                controller.SetSpawnTime(controller.GetSpawnTime() + (int)gameDifficulty + 1);
            }

            if (controller.GetSpawnableList()[0].tag == "Enemy" && controller.GetSpawnTime() > 3) {
                controller.SetSpawnTime(controller.GetSpawnTime() - (int)gameDifficulty - 1);
            }
        }
    }
}