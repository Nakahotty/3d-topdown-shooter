﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawnerController : MonoBehaviour
{
    [SerializeField] private List<GameObject> spawnableList;
    [SerializeField] private float spawnTime = 20; // Spawn an instance every {spawnTime} seconds
    [SerializeField] private float spawnRadius = 1;
    [SerializeField] private float life = 0; // Life alive in seconds, 0 if immortal
    [SerializeField] private bool isActive = true;
    private float elapsedTimeFromLastSpawn = 0;
    private float aliveTime = 0.001f;

    public void SetSpawnTime(float time) {
        spawnTime = time;
    }

    public void SetSpawnRadius(float radius) {
        spawnRadius = radius;
    }

    public void SetLife(float life) {
        this.life = life;
    }

    public float GetLife() {
        return life;
    }

    public float GetSpawnTime() {
        return spawnTime;
    }

    public float GetSpawnRadius() {
        return spawnRadius;
    }

    public List<GameObject> GetSpawnableList() {
        return spawnableList;
    }

    private void Update() {
        if (isActive) {
            elapsedTimeFromLastSpawn += Time.deltaTime;
            if (CanSpawn()) {
                elapsedTimeFromLastSpawn = 0;
                SpawnAnInstance();
            }

            aliveTime += Time.deltaTime;
            if (aliveTime > life && life != 0) {
                Destroy(gameObject);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collider) {
        if (collider.tag == "Player") {
            isActive = true;
        }
    }

    private void OnDrawGizmos() {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, spawnRadius);
    }

    private bool CanSpawn() {
        return elapsedTimeFromLastSpawn >= spawnTime;
    }

    private void SpawnAnInstance() {
        Vector3 spawnPos = CalculateRandomSpawnPosition();
        Instantiate(GetRandomSpawnable(), spawnPos, Quaternion.identity);
    }

    private GameObject GetRandomSpawnable() {
        return spawnableList[UnityEngine.Random.Range(0, spawnableList.Count - 1)];
    }

    private float GenerateRandomFloatInRange(float min, float max) {
        double range = (double)max - (double)min;
        double sample = UnityEngine.Random.value;
        double scaled = (sample * range) + min;
        float f = (float)scaled;
        return f;
    } 

    private Vector3 CalculateRandomSpawnPosition() {
        Vector3 spawnPosition = new Vector3(
            GenerateRandomFloatInRange(transform.position.x - spawnRadius, transform.position.x + spawnRadius),
            GenerateRandomFloatInRange(transform.position.y - spawnRadius, transform.position.y + spawnRadius),
            transform.position.z);
        while (Vector3.Distance(transform.position, spawnPosition) > spawnRadius) {
            spawnPosition.x = GenerateRandomFloatInRange(transform.position.x - spawnRadius, transform.position.x + spawnRadius);
            spawnPosition.y = GenerateRandomFloatInRange(transform.position.y - spawnRadius, transform.position.y + spawnRadius);
        }

        return new Vector3(spawnPosition.x, spawnPosition.y);
    }
}
