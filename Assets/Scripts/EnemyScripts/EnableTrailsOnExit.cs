﻿/// <summary>
/// A script that is used in the animator, when we go to the 
/// movement state of the enemy trails will start to spawn
/// using the TrailRenderer.
/// </summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableTrailsOnExit : StateMachineBehaviour
{
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        TrailRenderer[] allTrails = animator.GetComponentsInChildren<TrailRenderer>();
        foreach (TrailRenderer trail in allTrails)
        {
            trail.enabled = true;
        }
    }
}
