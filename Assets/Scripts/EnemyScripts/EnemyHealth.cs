﻿/// <summary>
/// Script that will handle the enemy health and damage effects from bullets
/// </summary>
using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using UnityEngine;

[RequireComponent(typeof(AIPath))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public class EnemyHealth : MonoBehaviour {
    private const float KnockbackTime = 0.1f;
    private const float BloodActiveTime = 0.5f;

    private Rigidbody2D enemyRB;
    private AIPath movementController;
    private bool hasEnemyDied = false;

    [Header("Blood effects")]
    [SerializeField] private Animator animator;
    [SerializeField] private GameObject bloodSplatterEffect;
    [SerializeField] private GameObject fatalBloodSplash;

    [Range(100, 200)] private int health = 100;

    public float GetHealth() {
        return health;
    }
    
    public void TakeDamage(int damage, float projectileForce, Vector3 projectilePosition) {
        if (!hasEnemyDied) {
            movementController.enabled = false;
            health -= damage;
            GetHit(projectileForce, projectilePosition);
            UpdateScore(damage);

            Invoke("KnockbackRecovery", KnockbackTime);
            StartCoroutine(SpillBlood(projectilePosition));

            if (hasEnemyDied) { 
                InstantiateFatalBloodSplash(projectilePosition);
                Die();
            }
        } 
    }
    
    private void Start() {
        movementController = GetComponent<AIPath>();
        enemyRB = GetComponent<Rigidbody2D>();
    }

    private void GetHit(float projectileForce, Vector3 projectilePosition) {
        if (health <= 0) {
            GetComponent<Collider2D>().enabled = false;
            hasEnemyDied = true;
            projectileForce *= 2f;
        }
        
        Vector2 angleOfForce = (transform.position - projectilePosition).normalized * projectileForce;
        enemyRB.AddForce(angleOfForce, ForceMode2D.Impulse);
    }

    private void InstantiateFatalBloodSplash(Vector3 projectilePosition) {
        float angle = GetBloodSplashAngle(projectilePosition);
        Instantiate(fatalBloodSplash, transform.position, Quaternion.AngleAxis(angle, Vector3.forward));
    }
    
    private IEnumerator SpillBlood(Vector3 projectilePosition) {
        float angle = GetBloodSplashAngle(projectilePosition);
        bloodSplatterEffect.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        bloodSplatterEffect.SetActive(true);
        yield return new WaitForSeconds(BloodActiveTime);
        bloodSplatterEffect.SetActive(false);
    }

    private float GetBloodSplashAngle(Vector3 projectilePosition) {
        Vector3 directionOfBlood = (projectilePosition - transform.position).normalized;
        float wantedAngle = Mathf.Atan2(directionOfBlood.y, directionOfBlood.x) * Mathf.Rad2Deg;
        return wantedAngle;
    }

    private void KnockbackRecovery() {
        enemyRB.velocity = Vector2.zero;
        if (!hasEnemyDied) {
            movementController.enabled = true;
        }
    }

    private void UpdateScore(int points) {
        if (hasEnemyDied) {
            points *= 2; // double the points on killing enemy.
        }

        ScoreManager.Instance.UpdateScore(points);
    }
    
    private void Die() {
        animator.SetTrigger("Die");
    }
}
