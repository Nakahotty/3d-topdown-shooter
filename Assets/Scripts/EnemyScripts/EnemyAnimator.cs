﻿/// <summary>
/// A class for handling the animations of our AI and making sure that 
/// each AI has a slight animation offset different from the others. 
/// It uses the AIPath velocity component, to check if an enemy is moving.
/// </summary>
using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using UnityEngine;

[RequireComponent(typeof(AIPath))]
public class EnemyAnimator : MonoBehaviour {
    private AIPath pathfinder;
    [SerializeField] private Animator animator;

    private void Start() {
        pathfinder = GetComponent<AIPath>();
        animator.SetFloat("Offset", Random.Range(0f, 1f));
    }
    
    private void Update() {
        AnimateMovement();
    }

    private void AnimateMovement() {
        animator.SetBool("Move", pathfinder.velocity.magnitude > 0.5f);
    }
}
