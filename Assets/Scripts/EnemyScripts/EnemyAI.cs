﻿/// <summary>
/// EnemyAI Scipt. Helper class that will always find a player for the AI to chase.
/// </summary>
using System;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using UnityEngine;

[RequireComponent(typeof(Seeker))]
public class EnemyAI : MonoBehaviour {
    private GameObject target;
    private Path path;
    private Seeker seeker;

    private void Awake() {
        seeker = GetComponent<Seeker>();
        target = GameObject.FindWithTag("Player");
    }

    private void Start() {
        InvokeRepeating("UpdatePath", 0.25f, 0.1f);
    }

    private void UpdatePath() {
        if (target == null) {
            target = GameObject.FindWithTag("Player");
        } 
        else if (seeker != null && seeker.IsDone()) {
            seeker.StartPath(transform.position, target.transform.position, OnPathComplete);
        }
    }

    private void OnPathComplete(Path pathToFollow) {
        if (!pathToFollow.error) {
            path = pathToFollow;
        } 
    }
}
