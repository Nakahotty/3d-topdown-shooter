﻿/// <summary>
/// This plays a role in disabling the AI scripts that may cause performance issues, 
/// also because we want the enemies to stay on the game when they die we disable the colliders and tweek the RB
/// </summary>
using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using UnityEngine;

public class EnemyDeath : StateMachineBehaviour
{
    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state.
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        MonoBehaviour[] scripts = animator.GetComponentsInParent<MonoBehaviour>();
        foreach (MonoBehaviour script in scripts) {
            script.enabled = false;
        }

        Rigidbody2D enemyRB = animator.GetComponentInParent<Rigidbody2D>();
        animator.GetComponent<SpriteRenderer>().sortingLayerName = "Ground";
        
        Collider2D hitboxCollider = animator.GetComponentInChildren<Collider2D>();
        hitboxCollider.enabled = false;

        enemyRB.velocity = Vector2.zero;
        enemyRB.isKinematic = true;
        enemyRB.freezeRotation = true;
    }
}
