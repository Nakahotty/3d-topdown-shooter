﻿/// <summary>
/// This class will be to the door tilemap and it
/// will open and close the doors when the player gets near them
/// The variable areLocked disables the openning function of the doors
/// when the player gets to the end territory
/// </summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorsController : MonoBehaviour
{
    private static bool areLocked = false;
    [SerializeField] private GameObject closedDoor;
    [SerializeField] private GameObject openedDoor;

    private void Start()
    {
        areLocked = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (gameObject.transform.childCount > 1 && transform.GetChild(0).gameObject)
            {
                if (!areLocked)
                {
                    StartCoroutine(ToggleDoor());
                }
            }
        }
    }

    private IEnumerator ToggleDoor()
    {
        if (gameObject.transform.childCount > 1)
        {
            OpenDoor();

            if (!areLocked)
            {
                yield return new WaitForSeconds(3f);
            } 
            else
            {
                yield return new WaitForSeconds(0.5f);
            }

            CloseDoor();
            yield return new WaitForSeconds(1f);
        }
    }

    private void OpenDoor()
    {
        if (!areLocked)
        {
            closedDoor.SetActive(false);
            openedDoor.SetActive(true);
            GetComponent<BoxCollider2D>().enabled = false;
            AudioManager.Instance.PlaySound("DoorOpenSound");
        }
        
        if (tag == "EndDoor")
        {
            Debug.Log("The doors are now locked!");
            areLocked = true;
        }
    }

    private void CloseDoor()
    {
        closedDoor.SetActive(true);
        openedDoor.SetActive(false);
        GetComponent<BoxCollider2D>().enabled = true;
    }
}
