﻿/// <summary>
/// This class handles all the sounds in the game
/// </summary>
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private Sound[] sounds;
    public static AudioManager Instance { get; set; }

    public void PlaySound(string name)
    {
        Sound soundToPlay = Array.Find(sounds, sound => sound.GetName() == name);
        if (soundToPlay == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }

        soundToPlay.Source.Play();
    }

    public void StopSound(string name)
    {
        Sound soundToStop = Array.Find(sounds, sound => sound.GetName() == name);
        if (soundToStop == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }

        soundToStop.Source.Stop();
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        Instance = this;

        foreach (Sound sound in sounds)
        {
            sound.Source = gameObject.AddComponent<AudioSource>();
            sound.Source.clip = sound.GetAudioClip();
            sound.Source.volume = sound.Volume;
            sound.Source.loop = sound.GetLoop();
        }
    }

    private void Start()
    {
        PlaySound("Background");
    }
}
