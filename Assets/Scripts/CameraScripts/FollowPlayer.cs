﻿/// <summary>
/// This class makes the virtual camera prefab always find a player 
/// so we don't get missing Game Object errors in the editor and in future development.
/// </summary>
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

[RequireComponent(typeof(CinemachineVirtualCamera))]
public class FollowPlayer : MonoBehaviour {
    private GameObject targetToFollow;
    private CinemachineVirtualCamera virtualCamera;
    
    private void Start() {
        virtualCamera = GetComponent<CinemachineVirtualCamera>();
        InvokeRepeating("SetVirtualCameraTarget", 0, 0.3f);
    }

    private void SetVirtualCameraTarget() {
        targetToFollow = GameObject.FindGameObjectWithTag("Player");

        if (targetToFollow != null) {
            virtualCamera.Follow = targetToFollow.transform;
            virtualCamera.LookAt = targetToFollow.transform;
            CancelInvoke("SetVirtualCameraTarget");
        }
    }
}
