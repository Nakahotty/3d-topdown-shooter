﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisablePlayerOnDeath : StateMachineBehaviour {
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        GameObject player = animator.gameObject.transform.parent.gameObject;

        TryDisable<WeaponHolder>(player);
        TryDisable<WeaponCollector>(player);
        TryDisable<PlayerMovement>(player);

        SpriteRenderer sr = player.GetComponentInChildren<SpriteRenderer>();
        if (sr) {
            sr.sortingLayerName = "Ground";
        }

        Rigidbody2D rb = player.GetComponentInChildren<Rigidbody2D>();
        if (rb) {
            rb.bodyType = RigidbodyType2D.Static;
        }
    }

    private void TryDisable<T>(GameObject go) where T : MonoBehaviour {
        T component = go.GetComponentInChildren<T>();
        if (component) {
            component.enabled = false;
        }
    }
}
