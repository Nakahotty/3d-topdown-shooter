﻿using UnityEngine;

public class PlayerDeathDetector : MonoBehaviour
{
    private int enemiesTouched = 0;

    [SerializeField] private float timeToDie = 1.3f;
    [SerializeField] private Animator playerAnimator;

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.CompareTag("Enemy")) {
            if (enemiesTouched == 0) {
                Invoke("Die", timeToDie);
            }

            enemiesTouched++;
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.gameObject.CompareTag("Enemy")) {
            enemiesTouched--;
            if (enemiesTouched == 0) {
                CancelInvoke("Die");
            }
        }
    }

    private void ShowGameOver() {
        ScoreManager.Instance.UpdateHighscore();
        GameObject.FindGameObjectWithTag("LevelMenus").GetComponent<LevelMenus>().ShowGameOver();
    }

    private void Die() {
        playerAnimator.SetTrigger("Die");
        Invoke("ShowGameOver", 2);
    }
}
