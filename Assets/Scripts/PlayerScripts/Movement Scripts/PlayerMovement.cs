﻿/// <summary>
/// This script will allow us to move the player. It uses a utillity script MovementComponent that rotates player to mouse position and moves his rigidbody
/// </summary>
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour {
    [SerializeField] private Transform rotationAxis; // Should be put on the shoulder of moving character. Put the rotation axis as a child empty gameObject under the character
    [SerializeField, Range(5f, 25f)] private float moveSpeed = 5f;
    [SerializeField] private Animator animator;

    private Rigidbody2D playerRB;

    public bool IsInputEnabled { get; set; }
    public Vector2 MovementInput { get; set; }
    public Vector2 MousePos { get; set; }

    private void Start() {
        IsInputEnabled = true;
        playerRB = GetComponent<Rigidbody2D>();
    }

    private void Update() {
        GetInput();
        AnimateMovement();
    }

    private void FixedUpdate() {
        MovementComponent.LookAt2D(playerRB, MousePos, rotationAxis);
        MovementComponent.Move(playerRB, MovementInput, moveSpeed);
    }

    private void AnimateMovement() {
        animator.SetBool("isMoving", Input.GetButton("Horizontal") || Input.GetButton("Vertical"));
    }

    private void GetInput() {
        if (IsInputEnabled) {
            MovementInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            MousePos = new Vector2(
                Camera.main.ScreenToWorldPoint(Input.mousePosition).x, 
                Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
        }
    }
}
