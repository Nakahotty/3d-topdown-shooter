﻿/// <summary>
///  Utillity class that moves the player towards the desired location via input, and rotates him to "LookAt" the mouse cursor.
/// </summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MovementComponent {
    private const float MovementMargin = 0.6f;
    
    public static void Move(Rigidbody2D rb, Vector2 input, float moveSpeed) {       
        rb.velocity = input.normalized * moveSpeed;
    }

    public static void LookAt2D(Rigidbody2D rb, Vector2 target, Transform rotationAxis) {
        Vector2 rotationAxisVector2 = rotationAxis.position;
        Vector2 lookDir = (target - rotationAxisVector2).normalized;

        float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg;

        if (Vector2.Distance(rotationAxisVector2, target) > MovementMargin) {
            rb.rotation = angle;
        }
    }
}
