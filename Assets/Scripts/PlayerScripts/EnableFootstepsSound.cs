﻿/// <summary>
/// This class plays the walking sound effect when the player is in moving state
/// </summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableFootstepsSound : StateMachineBehaviour
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (AudioManager.Instance != null)
        {
            AudioManager.Instance.PlaySound("WalkingSound");
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (AudioManager.Instance != null)
        {
            AudioManager.Instance.StopSound("WalkingSound");
        }
    }
}
