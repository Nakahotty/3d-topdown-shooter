﻿/// <summary>
/// UI for the ammo text on screen.
/// </summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class AmmoText : MonoBehaviour
{
    private WeaponHolder player;
    private Text text;

    private void OnEnable()
    {
        text = GetComponent<Text>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<WeaponHolder>();
        if (player)
        {
            player.SubscribeToAmmoChange(ChangeText);
        }
    }

    private void OnDisable()
    {
        if (player)
        {
            player.UnsubscribeToAmmoChange(ChangeText);
        }
    }

    private void ChangeText(int ammo)
    {
        if (ammo > 0)
        {
            text.text = ammo.ToString();
        }
        else if (ammo == 0)
        {
            text.text = "Out of ammo";
        }
        else
        {
            text.text = "∞";
        }
    }
}
