﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ScoreManager : AutoCleanupSingleton<ScoreManager> {
    private Text scoreLabel;
    public int ScoreValue { get; private set; }

    public void UpdateScore(int points) {
        ScoreValue += points;
        if (scoreLabel != null && points >= 0) {
            scoreLabel.text = "Score " + ScoreValue;
        }
    }

    public void UpdateHighscore() {
        if (ScoreValue > PlayerPrefs.GetInt("highscore")) {
            PlayerPrefs.SetInt("highscore", ScoreValue);
        }
    }
    
    private void Awake() {
        scoreLabel = GetComponent<Text>();
    }
}
