﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Projectile", menuName = "Weapons/Projectile")]
public class Projectile : ScriptableObject
{
    [field: SerializeField] public float AngleOffset { get; private set; }
    public float inaccuracy = 0;
    public Vector2 firePoint;
    public GameObject prefab;
}
