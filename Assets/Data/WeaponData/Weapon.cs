﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Weapons/Weapon")]
public class Weapon : ScriptableObject {
    [Header("Graphics")]
    public RuntimeAnimatorController runtimeAnimator;
    public Sprite pickupSprite;

    [Header("Shooting")]
    public int ammo = 36;
    public List<Projectile> shootingPattern;

    [field: SerializeField] public int Damage { get; private set; } = 5;
    [field: SerializeField] public float Force { get; private set; } = 5f;
    [field: SerializeField, Range(0.01f, 100)] public float Firerate { get; set; }
    public bool IsMeele => shootingPattern.Count <= 0; 
    [field: SerializeField] public string SoundName { get; private set; }
}
