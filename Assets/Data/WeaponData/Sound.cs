﻿/// <summary>
/// This class saves the name, audioclip, volume and source of a sound
/// </summary>
using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class Sound 
{
    [SerializeField, Range(0f, 1f)] public float Volume;
    [SerializeField, HideInInspector] public AudioSource Source;
    [SerializeField] private string soundName;
    [SerializeField] private AudioClip audioClip;
    [SerializeField] private bool isLooping;

    public string GetName() {
        return this.soundName;
    }

    public AudioClip GetAudioClip() {
        return this.audioClip;
    }

    public bool GetLoop()
    {
        return this.isLooping;
    }
}
