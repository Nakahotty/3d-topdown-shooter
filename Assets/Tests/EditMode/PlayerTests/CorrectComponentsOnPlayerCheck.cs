﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class CorrectComponentsOnPlayerCheck
    {
        [TestFixture]
        public class PlayerToTest {
            public static GameObject player;
        }

        [SetUp]
        public void SetUpTesting() {
            PlayerToTest.player = Resources.Load("Characters/Player") as GameObject;
        }

        [Test]
        public void PlayerActiveTest() {
            Assert.True(PlayerToTest.player.activeSelf, "Player object is disabled!");
        }

        [Test]
        public void PlayerTagTest() {
            Assert.That(PlayerToTest.player.tag.Equals("Player"), "Player Object must be with tag \"Player\"!");
        }

        [Test]
        public void MovementScriptComponentTest() {
            PlayerMovement componentToTest = PlayerToTest.player.GetComponent<PlayerMovement>();
            Assert.IsNotNull(componentToTest, "Player object doesn't have a PlayerMovement component attached to him!");
            Assert.That(componentToTest.enabled, "PlayerMovement component is disabled!");
        }

        [Test]
        public void ChildrenOfDataCorrectComponentsTest() {
            Assert.IsNotNull(PlayerToTest.player.transform.Find("PlayerRenderer").GetComponent<Animator>(), 
                "Player object should have an animator attached to a child called \"PlayerRenderer!\"");

            Assert.IsNotNull(PlayerToTest.player.transform.Find("PlayerRenderer").GetComponent<SpriteRenderer>(), 
                "Player object should have a sprite renderer to a child called \"PlayerRenderer!\"");

            Assert.IsNotNull(PlayerToTest.player.transform.Find("PlayerRenderer").Find("MeleeHitbox").GetComponent<DamageDealer>(), 
                "Player child object \"PlayerRenderer\" called \"MeleeHitbox\" should have a DamageDealer component in it!");
        }

        [Test]
        public void PlayerPhysicsComponentTest() {
            Assert.IsNotNull(PlayerToTest.player.GetComponent<Rigidbody2D>(), "Player object doesn't have a Rigidbody2D");
            Assert.IsNotNull(PlayerToTest.player.GetComponent<Collider2D>(), "Player object doesn't have a Collider2D");
        }
    }
}
