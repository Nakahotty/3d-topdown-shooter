﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class DamageDealersCorrectDataTest {
        [TestFixture]
        public class DamageDealersToTest {
            public static GameObject bullet;
            public static GameObject meleeHitbox;
        }

        [SetUp]
        public void SetUpTesting() {
            DamageDealersToTest.bullet = Resources.Load("Armory/DamageDealers/Bullet") as GameObject;
            DamageDealersToTest.meleeHitbox = Resources.Load("Armory/DamageDealers/MeleeHitbox") as GameObject;
        }

        [Test]
        public void BulletCorrectColliderComponentTest() {
            Collider2D colliderForTesting = DamageDealersToTest.bullet.GetComponent<Collider2D>();
            Assert.IsNotNull(colliderForTesting, "Bullets must have a collider!");
            Assert.That(colliderForTesting.enabled, "Bullet collider must be enabled!");
            Assert.That(colliderForTesting.isTrigger, "Bullet collider should be Trigger!");
        }

        [Test]
        public void BulletCorrectScriptComponentTest() {
            RangedDamageDealer componentForTesting = DamageDealersToTest.bullet.GetComponent<RangedDamageDealer>();
            Assert.IsNotNull(componentForTesting, "Bullet prefab should always have a RangeDamageDealer component attached! It's the component that makes the shooting possible!");
            Assert.That(componentForTesting.enabled, "RangeRamageDealer is not enabled!");
        }

        [Test]
        public void BulletRigidBodyComponentTest() {
            Rigidbody2D rigidBodyForTesting = DamageDealersToTest.bullet.GetComponent<Rigidbody2D>();
            Assert.IsNotNull(rigidBodyForTesting);
            Assert.AreEqual(rigidBodyForTesting.collisionDetectionMode, CollisionDetectionMode2D.Continuous, "Bullets should have continues collision detection mode!");
        }

        [Test]
        public void MeleeCorrectColliderComponentTest() {
            Collider2D colliderForTesting = DamageDealersToTest.meleeHitbox.GetComponent<Collider2D>();
            Assert.IsNotNull(colliderForTesting, "Melee hitbox must have a collider!");
            Assert.That(colliderForTesting.enabled, "Melee hitbox collider must be enabled!");
            Assert.That(colliderForTesting.isTrigger, "Melee hitbox collider should be Trigger!");
        }

        [Test]
        public void MeleeCorrectScriptComponentTest() {
            DamageDealer componentForTesting = DamageDealersToTest.meleeHitbox.GetComponent<DamageDealer>();
            Assert.IsNotNull(componentForTesting, "Bullet prefab should always have a DamageDealer component attached! It's the component that makes the hitting possible!");
            Assert.That(componentForTesting.enabled, "RamageDealer is not enabled!");
        }
    }
}
