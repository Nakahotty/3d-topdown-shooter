﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;

namespace Tests
{
    public class WeaponCorrectDataTests
    {
        [TestFixture]
        public class WeaponsToTest {
            public static GameObject[] weapons;
        }

        [SetUp]
        public void SetUpTesting() {
            WeaponsToTest.weapons = Resources.LoadAll("Armory/WeaponPrefabs", typeof(GameObject)).Cast<GameObject>().ToArray();
        }

        [Test]
        public void CheckForPickupComponentOnEveryWeapon() {
            foreach (GameObject weapon in WeaponsToTest.weapons) {
                Assert.IsNotNull(weapon.GetComponent<Pickup>(), "Every weapon should have a pickup component!");
                Assert.AreNotEqual(weapon.GetComponent<Pickup>().GetWeapon().name, "Unarmed", "A weapon has \"Unarmed\" set for Weapon variable!");
            }
        }

        [Test]
        public void FirerateGreaterThanZeroOnEveryWeaponTest() {
            foreach(GameObject weapon in WeaponsToTest.weapons) {
                Assert.Positive(weapon.GetComponent<Pickup>().GetWeapon().Firerate, "Firerate should be a positive number!");
            }
        }

        [Test]
        public void DamageGreaterThanZeroOnEveryWeaponTest() {
            foreach (GameObject weapon in WeaponsToTest.weapons) {
                Assert.Positive(weapon.GetComponent<Pickup>().GetWeapon().Damage, "Damage should be a positive number!");
                Assert.Positive(weapon.GetComponent<Pickup>().GetWeapon().Force, "Force should be a positive number!");
            }
        }

        [Test]
        public void ForceGreaterThanZeroOnEveryWeaponTest() {
            foreach (GameObject weapon in WeaponsToTest.weapons) {
                Assert.Positive(weapon.GetComponent<Pickup>().GetWeapon().Force, "Force should be a positive number!");
            }
        }

        [Test]
        public void ColliderComponentOnEveryWeaponTest() {
            foreach (GameObject weapon in WeaponsToTest.weapons) {
                Assert.NotNull(weapon.GetComponent<Collider2D>(), "Weapons must have colliders!");
                Assert.That(weapon.GetComponent<Collider2D>().isTrigger, "Weapon colliders should be trigger!");
            }
        }

        [TearDown]
        public void Clean() {
            Array.Clear(WeaponsToTest.weapons, 0, WeaponsToTest.weapons.Length);
        }
    }
}
