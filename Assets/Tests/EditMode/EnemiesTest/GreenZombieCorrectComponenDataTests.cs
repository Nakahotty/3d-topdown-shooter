﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using Pathfinding;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class GreenZombieCorrectComponentDataTests {
        [TestFixture]
        public class ZombieToTest {
            public static GameObject greenZombie;
        }

        [SetUp]
        public void SetUpTesting() {
            ZombieToTest.greenZombie = Resources.Load("Characters/GreenZombie") as GameObject;
        }

        [Test]
        public void PhysicsComponentsTest() {
            Assert.IsNotNull(ZombieToTest.greenZombie.GetComponent<Rigidbody2D>(), "Zombie object doesn't have a Rigidbody2D!");
            Assert.IsNotNull(ZombieToTest.greenZombie.GetComponent<Collider2D>(), "Zombie object doesn't have a Collider2D!");
            Assert.That(ZombieToTest.greenZombie.GetComponent<Collider2D>().enabled, "Zombie object's Collider2D is disabled!");
        }

        [Test]
        public void AI_ComponentsTest() {
            Assert.IsNotNull(ZombieToTest.greenZombie.GetComponent<Seeker>(), "Zombie object doesn't have a Seeker component!");
            Assert.IsNotNull(ZombieToTest.greenZombie.GetComponent<AIPath>(), "Zombie object doesn't have a AIPath component!");
            Assert.IsNotNull(ZombieToTest.greenZombie.GetComponent<EnemyAI>(), "Zombie object doesn't have a EnemyAI component!");
            Assert.That(ZombieToTest.greenZombie.GetComponent<EnemyAI>().enabled, "Zombie object's EnemyAI component is disabled!");
        }

        [Test]
        public void EnemyHealthComponentTest() {
            Assert.IsNotNull(ZombieToTest.greenZombie.GetComponent<EnemyHealth>(), "Zombie object doesn't have a EnemyHealth component!");
            Assert.That(ZombieToTest.greenZombie.GetComponent<EnemyHealth>().enabled, "Zombie object's EnemyHealth component is disabled!");
        }
    }
}
