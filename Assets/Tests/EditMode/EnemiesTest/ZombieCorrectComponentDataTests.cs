﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using Pathfinding;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class ZombieCorrectComponentDataTests
    {
        [TestFixture]
        public class ZombieToTest {
            public static GameObject zombie;
        }

        [SetUp]
        public void SetUpTesting() {
            ZombieToTest.zombie = Resources.Load("Characters/Zombie") as GameObject;
        }

        [Test]
        public void PhysicsComponentsTest() {
            Assert.IsNotNull(ZombieToTest.zombie.GetComponent<Rigidbody2D>(), "Zombie object doesn't have a Rigidbody2D!");
            Assert.IsNotNull(ZombieToTest.zombie.GetComponent<Collider2D>(), "Zombie object doesn't have a Collider2D!");
            Assert.That(ZombieToTest.zombie.GetComponent<Collider2D>().enabled, "Zombie object's Collider2D is disabled!");
        }

        [Test]
        public void AI_ComponentsTest() {
            Assert.IsNotNull(ZombieToTest.zombie.GetComponent<Seeker>(), "Zombie object doesn't have a Seeker component!");
            Assert.IsNotNull(ZombieToTest.zombie.GetComponent<AIPath>(), "Zombie object doesn't have a AIPath component!");
            Assert.IsNotNull(ZombieToTest.zombie.GetComponent<EnemyAI>(), "Zombie object doesn't have a EnemyAI component!");
            Assert.That(ZombieToTest.zombie.GetComponent<EnemyAI>().enabled, "Zombie object's EnemyAI component is disabled!");
        }

        [Test]
        public void EnemyHealthComponentTest() {
            Assert.IsNotNull(ZombieToTest.zombie.GetComponent<EnemyHealth>(), "Zombie object doesn't have a EnemyHealth component!");
            Assert.That(ZombieToTest.zombie.GetComponent<EnemyHealth>().enabled, "Zombie object's EnemyHealth component is disabled!");
        }
    }
}
