﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class ZombieDamagedTests
    {
        [TestFixture]
        public class Enemy {
            public static GameObject enemy;
            public static EnemyHealth enemyHealth;
        }

        [SetUp]
        public void SetUpTesting() {
            Enemy.enemy = Object.Instantiate(Resources.Load("Characters/Zombie") as GameObject, new Vector2(0, 0), Quaternion.identity);
            Enemy.enemyHealth = Enemy.enemy.GetComponent<EnemyHealth>();
            Enemy.enemy.GetComponent<EnemyAI>().enabled = false; // we won't spawn player so we don't need this script.
        }

        [UnityTest]
        public IEnumerator ZombieShouldGetHitFromTheFront()
        {
            Vector2 originalPos = Enemy.enemy.transform.position;
            Enemy.enemyHealth.TakeDamage(5, 5f, Vector2.up);

            yield return new WaitForSeconds(0.1f);
            Vector2 expectedPos = Enemy.enemy.transform.position;
            Assert.That(expectedPos.y < originalPos.y && expectedPos.x == originalPos.x);
        }

        [UnityTest]
        public IEnumerator ZombieShouldGetHitFromTheBack() {
            Vector2 originalPos = Enemy.enemy.transform.position;
            Enemy.enemyHealth.TakeDamage(5, 5f, Vector2.down);

            yield return new WaitForSeconds(0.1f);
            Vector2 expectedPos = Enemy.enemy.transform.position;
            Assert.That(expectedPos.y > originalPos.y && expectedPos.x == originalPos.x);
        }

        [UnityTest]
        public IEnumerator  ZombieShouldGetFromTheRight() {
            Vector2 originalPos = Enemy.enemy.transform.position;
            Enemy.enemyHealth.TakeDamage(5, 5f, Vector2.right);

            yield return new WaitForSeconds(0.1f);
            Vector2 expectedPos = Enemy.enemy.transform.position;
            Assert.That(expectedPos.x < originalPos.x && expectedPos.y == originalPos.y);
        }

        [UnityTest]
        public IEnumerator ZombieShouldGetHitFromTheLeft() {
            Vector2 originalPos = Enemy.enemy.transform.position;
            Enemy.enemyHealth.TakeDamage(5, 5f, Vector2.left);

            yield return new WaitForSeconds(0.1f);
            Vector2 expectedPos = Enemy.enemy.transform.position;
            Assert.That(expectedPos.x > originalPos.x && expectedPos.y == originalPos.y);
        }

        [UnityTest]
        public IEnumerator ZombieShouldGetHitFromTopRight() {
            Vector2 originalPos = Enemy.enemy.transform.position;
            Enemy.enemyHealth.TakeDamage(5, 5f, Vector2.up + Vector2.right);

            yield return new WaitForSeconds(0.1f);
            Vector2 expectedPos = Enemy.enemy.transform.position;
            Assert.That(expectedPos.y < originalPos.y && expectedPos.x < originalPos.x);
        }

        [UnityTest]
        public IEnumerator ZombieGettingHitFromTopLeft() {
            Vector2 originalPos = Enemy.enemy.transform.position;
            Enemy.enemyHealth.TakeDamage(5, 5f, Vector2.up + Vector2.left);

            yield return new WaitForSeconds(0.1f);
            Vector2 expectedPos = Enemy.enemy.transform.position;
            Assert.That(expectedPos.y < originalPos.y && expectedPos.x > originalPos.x);
        }

        [UnityTest]
        public IEnumerator ZombieGettingHitFromBottomLeft() {
            Vector2 originalPos = Enemy.enemy.transform.position;
            Enemy.enemyHealth.TakeDamage(5, 5f, Vector2.down + Vector2.left);

            yield return new WaitForSeconds(0.1f);
            Vector2 expectedPos = Enemy.enemy.transform.position;
            Assert.That(expectedPos.y > originalPos.y && expectedPos.x > originalPos.x);
        }

        [UnityTest]
        public IEnumerator ZombieGettingHitFromBottomRight() {
            Vector2 originalPos = Enemy.enemy.transform.position;
            Enemy.enemyHealth.TakeDamage(5, 5f, Vector2.down + Vector2.right);

            yield return new WaitForSeconds(0.1f);
            Vector2 expectedPos = Enemy.enemy.transform.position;
            Assert.That(expectedPos.y > originalPos.y && expectedPos.x < originalPos.x);
        }

        [UnityTest]
        public IEnumerator ZombieGettingHitFatally() {
            Vector2 originalPos = Enemy.enemy.transform.position;
            Enemy.enemyHealth.TakeDamage(5, 10f, Vector2.up);

            yield return new WaitForSeconds(0.2f);
            Vector2 pushedPosition = Enemy.enemy.transform.position;
            float knockbackDistanceOnNormalHit = Vector2.Distance(originalPos, pushedPosition);

            Enemy.enemyHealth.TakeDamage(1000, 10f, Vector2.up);
            yield return new WaitForSeconds(0.2f);
            Vector2 pushedPositionAfterDeath = Enemy.enemy.transform.position;
            float knockbackDistanceOnFatalHit = Vector2.Distance(pushedPosition, pushedPositionAfterDeath);

            Assert.That(knockbackDistanceOnFatalHit > knockbackDistanceOnNormalHit, "On fatal hit we should have a stronger knockback!");
        }

        [UnityTest]
        public IEnumerator ZombieGettingHitFatallyColliderTest() {
            Enemy.enemyHealth.TakeDamage(1000, 10f, Vector2.up);
            yield return new WaitForSeconds(0.1f);
            Assert.IsFalse(Enemy.enemy.GetComponent<Collider2D>().enabled, "Main collider should be disabled on death!");
        }

        [UnityTest]
        public IEnumerator ZombieHealthDecreaseTest() {
            float originalHealth = Enemy.enemyHealth.GetHealth();
            Enemy.enemy.GetComponent<EnemyHealth>().TakeDamage(5, 10f, Vector2.up);
            yield return new WaitForSeconds(0.1f);
            float expectedHealth = Enemy.enemyHealth.GetHealth();
            Assert.Greater(originalHealth, expectedHealth, "Enemy health variable doesn't change on damage taken!");
        }
        
        [TearDown]
        public void Clean() {
            Object.Destroy(Enemy.enemy);
        }
    }
}
