﻿/// <summary>
/// Unit tests checking if the AI moves before spawn (it shouldn't) 
/// and if it moves after it is spawned (it should)
/// </summary>
namespace Tests {
 using NUnit.Framework;
 using System.Collections;
 using UnityEngine.SceneManagement;
 using UnityEngine.TestTools;
 using UnityEngine;
   
    public class EnemyMovementTest {
        [TestFixture]
        public class Enemy {
            public static GameObject enemy;
            public static GameObject astar;
        }
    
        [SetUp]
        public void SetUpTesting() {
            Enemy.enemy = Object.Instantiate(Resources.Load("Characters/Zombie") as GameObject, new Vector2(0, 0), Quaternion.identity);
            Enemy.astar = Object.Instantiate(Resources.Load("Others/AStarPathfinder") as GameObject);
        }
    
        [UnityTest]
        public IEnumerator MovingRightAfterSpawn() {
            Vector3 enemyStartingPos = Enemy.enemy.transform.position;
    
            GameObject player = new GameObject();
            player.transform.position = new Vector3(0, 10, 0);
            player.tag = "Player";
    
            Vector3 enemyExpectedChangedPos = Enemy.enemy.transform.position;
            Assert.AreEqual(enemyStartingPos, enemyExpectedChangedPos);
           
            yield return new WaitForSeconds(0.5f);
            enemyExpectedChangedPos = Enemy.enemy.transform.position;
    
            Assert.AreNotEqual(enemyStartingPos, enemyExpectedChangedPos);
        }
    
        [TearDown]
        public void Clean() {
            Object.Destroy(Enemy.enemy);
        }
    }
}
