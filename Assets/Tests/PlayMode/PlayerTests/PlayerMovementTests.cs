﻿/// <summary>
/// Unit tests checking if the player moves towards the camera and in the correct directions
/// Made a class InputManager to disable all input so the player object doesn't get some random input
/// that will ruin the test.
/// </summary>
using NUnit.Framework;
using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;
namespace Tests {
    public class PlayerMovementTests {

        [TestFixture]
        public class Player {
            public static GameObject player;
            public static GameObject camera;
            public static PlayerMovement playerMovement;
        }

        [SetUp]
        public void SetUpTesting() {
            Player.player = Object.Instantiate(Resources.Load("Characters/Player") as GameObject, new Vector2(0, 0), Quaternion.identity);
            Player.camera = Object.Instantiate(Resources.Load("Others/Main Camera") as GameObject, new Vector2(0, 0), Quaternion.identity);
            Player.playerMovement = Player.player.GetComponent<PlayerMovement>();
        }


        [UnityTest]
        public IEnumerator MovingUpTest() {
            float originalYPos = Player.player.transform.position.y;
            Player.playerMovement.IsInputEnabled = false;
            yield return new WaitForSeconds(0.1f);

            Player.playerMovement.MovementInput = new Vector2(0, 100);

            yield return new WaitForSeconds(0.1f);

            float newYPos = Player.player.transform.position.y;

            Assert.That(newYPos > originalYPos);
        }

        [UnityTest]
        public IEnumerator MovingDownTest() {
            float originalYPos = Player.player.transform.position.y;
            Player.playerMovement.IsInputEnabled = false;
            yield return new WaitForSeconds(0.1f);

            Player.playerMovement.MovementInput = new Vector2(0, -100);

            yield return new WaitForSeconds(0.1f);

            float newYPos = Player.player.transform.position.y;

            Assert.That(newYPos < originalYPos);
        }

        [UnityTest]
        public IEnumerator MovingRightTest() {
            float originalXPos = Player.player.transform.position.x;
            Player.playerMovement.IsInputEnabled = false;
            yield return new WaitForSeconds(0.1f);

            Player.playerMovement.MovementInput = new Vector2(100, 0);

            yield return new WaitForSeconds(0.1f);

            float newXPos = Player.player.transform.position.x;

            Assert.That(newXPos > originalXPos);
        }

        [UnityTest]
        public IEnumerator MovingLeftTest() {
            float originalXPos = Player.player.transform.position.x;
            Player.playerMovement.IsInputEnabled = false;
            yield return new WaitForSeconds(0.1f);

            Player.playerMovement.MovementInput = new Vector2(-100, 0);

            yield return new WaitForSeconds(0.1f);

            float newXPos = Player.player.transform.position.x;

            Assert.That(newXPos < originalXPos);
        }

        [TearDown]
        public void Clean() {
            GameObject.Destroy(Player.player);
            GameObject.Destroy(Player.camera);
        }
    }
}
