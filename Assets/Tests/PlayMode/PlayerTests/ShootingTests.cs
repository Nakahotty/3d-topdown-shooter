﻿/// <summary>
/// Unit tests checking if the player picks up the correct weapon (the one that is closest to him)
/// </summary>
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests {
    public class ShootingTests {
        [TestFixture]
        public class Player {
            public static GameObject player;

            public static WeaponHolder weaponHolder;

            public static void ChangePlayerPosition(float x, float y) {
                player.transform.position = new Vector2(x, y);
            }
        }

        [SetUp]
        public void SetUpTesting() {
            Player.player = Object.Instantiate(Resources.Load("Characters/Player") as GameObject, new Vector2(0, 0), Quaternion.identity);
            Player.weaponHolder = Player.player.GetComponentInChildren<WeaponHolder>();
            GameObject.Destroy(Player.player.GetComponent<PlayerMovement>());
        }

        [TearDown]
        public void Clean() {
            Object.Destroy(Player.player);
        }

        [UnityTest]
        public IEnumerator Shooting_ShouldReduceAmmo() {
            Weapon weapon = Player.weaponHolder.GetCurrentlyHoldedWeapon();
            weapon.ammo = 4;
            weapon.Firerate = 100;
            Player.weaponHolder.PickUpWeapon(weapon); // to refresh the firerate

            yield return new WaitForSeconds(0.1f);
            Player.weaponHolder.Shoot();
            Assert.AreEqual(4 - 1, Player.weaponHolder.GetCurrentlyHoldedWeapon().ammo);
            yield return 0;
        }

        [UnityTest]
        public IEnumerator Shooting_ShouldNotReduceBelowZero() {
            Weapon weapon = Player.weaponHolder.GetCurrentlyHoldedWeapon();
            weapon.ammo = 1;
            weapon.Firerate = 100;
            Player.weaponHolder.PickUpWeapon(weapon); // to refresh the firerate

            yield return new WaitForSeconds(0.1f);
            Player.weaponHolder.Shoot();
            Assert.AreEqual(0, Player.weaponHolder.GetCurrentlyHoldedWeapon().ammo);
            yield return 0;
        }

        [UnityTest]
        public IEnumerator Shooting_ShouldNotShootOnCooldown() {
            Weapon weapon = Player.weaponHolder.GetCurrentlyHoldedWeapon();
            weapon.ammo = 3;
            weapon.Firerate = 100;
            Player.weaponHolder.PickUpWeapon(weapon); // to refresh the firerate

            yield return new WaitForSeconds(0.1f);
            Player.weaponHolder.Shoot();
            Player.weaponHolder.Shoot();
            Assert.AreEqual(3-1, Player.weaponHolder.GetCurrentlyHoldedWeapon().ammo);
            yield return 0;
        }

        [UnityTest]
        public IEnumerator Melee_ShouldKnockbackZombieWithMeleeHammerAttack() {
            GameObject enemy = Object.Instantiate(Resources.Load("Characters/Zombie") as GameObject, new Vector2(Player.player.transform.position.x + 1.5f, Player.player.transform.position.y), Quaternion.identity);
            enemy.GetComponent<EnemyAI>().enabled = false;

            GameObject hammer = Resources.Load("Armory/WeaponPrefabs/Hammer") as GameObject;
            Player.weaponHolder.PickUpWeapon(hammer.GetComponent<Pickup>().GetWeapon());

            Vector2 originalEnemyPosition = enemy.transform.position;
            float originalEnemyHealth = enemy.GetComponent<EnemyHealth>().GetHealth();
            yield return new WaitForSeconds(0.5f);

            Player.weaponHolder.Shoot();
            yield return new WaitForSeconds(0.1f);

            Vector2 expectedEnemyPoistion = enemy.transform.position;
            float expectedEnemyHealth = enemy.GetComponent<EnemyHealth>().GetHealth();

            Assert.AreNotSame(originalEnemyPosition, expectedEnemyPoistion, "Hammer doesn't apply any force to enemy!");
            Assert.Greater(originalEnemyHealth, expectedEnemyHealth, "Hammer doesn't deal any damage to enemy!");
            Object.Destroy(enemy);
        }

        [UnityTest]
        public IEnumerator Melee_ShouldKnockbackZombieWithUnarmedMeleeAttack() {
            GameObject enemy = Object.Instantiate(Resources.Load("Characters/Zombie") as GameObject, new Vector2(Player.player.transform.position.x + 1.5f, Player.player.transform.position.y), Quaternion.identity);
            enemy.GetComponent<EnemyAI>().enabled = false;

            Player.weaponHolder.DropWeapon();

            Vector2 originalEnemyPosition = enemy.transform.position;
            float originalEnemyHealth = enemy.GetComponent<EnemyHealth>().GetHealth();
            yield return new WaitForSeconds(0.5f);

            Player.weaponHolder.Shoot();
            yield return new WaitForSeconds(0.2f);

            Vector2 expectedEnemyPoistion = enemy.transform.position;
            float expectedEnemyHealth = enemy.GetComponent<EnemyHealth>().GetHealth();

            Assert.AreNotSame(originalEnemyPosition, expectedEnemyPoistion, "Unarmed punch doesn't apply any force to enemy!");
            Assert.Greater(originalEnemyHealth, expectedEnemyHealth, "Unarmed punch doesn't deal any damage to enemy!");
            Object.Destroy(enemy);
        }

        [UnityTest]
        public IEnumerator Melee_ShouldKnockbackGreenZombieWithMeleeHammerAttack() {
            GameObject enemy = Object.Instantiate(Resources.Load("Characters/GreenZombie") as GameObject, new Vector2(Player.player.transform.position.x + 1f, Player.player.transform.position.y), Quaternion.identity);
            enemy.GetComponent<EnemyAI>().enabled = false;

            GameObject hammer = Resources.Load("Armory/WeaponPrefabs/Hammer") as GameObject;
            Player.weaponHolder.PickUpWeapon(hammer.GetComponent<Pickup>().GetWeapon());

            Vector2 originalEnemyPosition = enemy.transform.position;
            float originalEnemyHealth = enemy.GetComponent<EnemyHealth>().GetHealth();
            yield return new WaitForSeconds(0.5f);

            Player.weaponHolder.Shoot();
            yield return new WaitForSeconds(0.1f);

            Vector2 expectedEnemyPoistion = enemy.transform.position;
            float expectedEnemyHealth = enemy.GetComponent<EnemyHealth>().GetHealth();

            Assert.AreNotSame(originalEnemyPosition, expectedEnemyPoistion, "Hammer doesn't apply any force to enemy!");
            Assert.Greater(originalEnemyHealth, expectedEnemyHealth, "Hammer doesn't deal any damage to enemy!");
            Object.Destroy(enemy);
        }

        [UnityTest]
        public IEnumerator Melee_ShouldKnockbackGreenZombieWithUnarmedMeleeAttack() {
            GameObject enemy = Object.Instantiate(Resources.Load("Characters/GreenZombie") as GameObject, new Vector2(Player.player.transform.position.x + 1f, Player.player.transform.position.y), Quaternion.identity);
            enemy.GetComponent<EnemyAI>().enabled = false;

            Player.weaponHolder.DropWeapon();

            Vector2 originalEnemyPosition = enemy.transform.position;
            float originalEnemyHealth = enemy.GetComponent<EnemyHealth>().GetHealth();
            yield return new WaitForSeconds(0.5f);

            Player.weaponHolder.Shoot();
            yield return new WaitForSeconds(0.2f);

            Vector2 expectedEnemyPoistion = enemy.transform.position;
            float expectedEnemyHealth = enemy.GetComponent<EnemyHealth>().GetHealth();

            Assert.AreNotSame(originalEnemyPosition, expectedEnemyPoistion, "Unarmed punch doesn't apply any force to enemy!");
            Assert.Greater(originalEnemyHealth, expectedEnemyHealth, "Unarmed punch doesn't deal any damage to enemy!");
            Object.Destroy(enemy);
        }
    }
}
