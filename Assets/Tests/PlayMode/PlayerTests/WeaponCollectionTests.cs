﻿/// <summary>
/// Unit tests checking if the player picks up the correct weapon (the one that is closest to him)
/// </summary>
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class WeaponCollectionTests
    {
        [TestFixture]
        public class Player {
            public static GameObject player;
            public static GameObject shotgunWeapon;
            public static GameObject uziWeapon;
            public static GameObject magnumWeapon;
            public static GameObject hammerWeapon;

            public static GameObject camera;
            public static WeaponCollector weaponCollector;
            public static WeaponHolder weaponHolder;

            public static void ChangePlayerPosition(float x, float y) {
                player.transform.position = new Vector2(x, y);
            }

            public static void InstantiateShotgun(float x, float y) {
                shotgunWeapon = Object.Instantiate
                    (Resources.Load("Armory/WeaponPrefabs/Shotgun") as GameObject, new Vector2(x, y), Quaternion.identity);
            }

            public static void InstantiateUzi(float x, float y) {
                uziWeapon = Object.Instantiate
                    (Resources.Load("Armory/WeaponPrefabs/Uzi") as GameObject, new Vector2(x, y), Quaternion.identity);
            }

            public static void InstantiateMagnum(float x, float y) {
                magnumWeapon = Object.Instantiate
                    (Resources.Load("Armory/WeaponPrefabs/Magnum") as GameObject, new Vector2(x, y), Quaternion.identity);
            }

            public static void InstantiateHammer(float x, float y) {
                hammerWeapon = Object.Instantiate
                    (Resources.Load("Armory/WeaponPrefabs/Hammer") as GameObject, new Vector2(x, y), Quaternion.identity);
            }

            public static void ClearWeaponFromScreen() {
                if (shotgunWeapon != null) {
                    Object.Destroy(shotgunWeapon);
                }

                if (uziWeapon != null) {
                    Object.Destroy(uziWeapon);
                }

                if (magnumWeapon != null) {
                    Object.Destroy(magnumWeapon);
                }

                if (hammerWeapon != null) {
                    Object.Destroy(hammerWeapon);
                }
            }
        }

        [SetUp]
        public void SetUpTesting() {
            Player.player = Object.Instantiate(Resources.Load("Characters/Player") as GameObject, new Vector2(0, 0), Quaternion.identity);
            Player.camera = Object.Instantiate(Resources.Load("Others/Main Camera") as GameObject, new Vector2(0, 0), Quaternion.identity);
            Player.weaponCollector = Player.player.GetComponentInChildren<WeaponCollector>();
            Player.weaponHolder = Player.player.GetComponentInChildren<WeaponHolder>();
        }

        [UnityTest]
        public IEnumerator Test1_PickingUpNothingTest() {
            Player.ChangePlayerPosition(0, 0);
            yield return new WaitForSeconds(0.1f);
            Weapon originalWeapon = Player.weaponHolder.GetCurrentlyHoldedWeapon();
            Player.weaponCollector.CollectWeapon();
            Assert.AreEqual(Player.weaponHolder.GetCurrentlyHoldedWeapon(), originalWeapon);

            yield return 0;
        }

        [UnityTest]
        public IEnumerator Test2_PickingUpShotgunTest() {
            Player.ChangePlayerPosition(0, -5);
            Player.InstantiateShotgun(0, -5.2f);

            yield return new WaitForSeconds(0.1f);
            
            Player.weaponCollector.CollectWeapon();
            Assert.IsNotNull(Player.weaponHolder.GetCurrentlyHoldedWeapon());
            Assert.AreEqual(Player.weaponHolder.GetCurrentlyHoldedWeapon().runtimeAnimator.name, "ShotgunController");
        }

        [UnityTest]
        public IEnumerator Test3_PickingUpUziTest() {
            Player.ChangePlayerPosition(0, -7);
            Player.InstantiateUzi(0, -7.2f);

            yield return new WaitForSeconds(0.1f);

            Player.weaponCollector.CollectWeapon();
            Assert.IsNotNull(Player.weaponHolder.GetCurrentlyHoldedWeapon());
            Assert.AreEqual(Player.weaponHolder.GetCurrentlyHoldedWeapon().runtimeAnimator.name, "UziController");
        }

        [UnityTest]
        public IEnumerator Test4_PickingUpMagnumTest() {
            Player.ChangePlayerPosition(0, -9);
            Player.InstantiateMagnum(0, -9.2f);

            yield return new WaitForSeconds(0.1f);

            Player.weaponCollector.CollectWeapon();
            Assert.IsNotNull(Player.weaponHolder.GetCurrentlyHoldedWeapon());
            Assert.AreEqual(Player.weaponHolder.GetCurrentlyHoldedWeapon().runtimeAnimator.name, "MagnumController");
        }

        [UnityTest]
        public IEnumerator Test5_PickingUpHammerTest() {
            Player.ChangePlayerPosition(0, -11);
            Player.InstantiateHammer(0, -11.2f);

            yield return new WaitForSeconds(0.1f);

            Player.weaponCollector.CollectWeapon();
            Assert.IsNotNull(Player.weaponHolder.GetCurrentlyHoldedWeapon());
            Assert.AreEqual(Player.weaponHolder.GetCurrentlyHoldedWeapon().runtimeAnimator.name, "HammerController");
        }

        [UnityTest]
        public IEnumerator Test6_TheNearestWeaponTest()
        {
            Player.ChangePlayerPosition(0, -13);
            Player.InstantiateShotgun(0.5f, -13);
            Player.InstantiateUzi(1, -13);

            yield return new WaitForSeconds(0.1f);

            float distanceToshotgunWeapon  = (Player.player.transform.position - Player.shotgunWeapon.transform.position).sqrMagnitude;
            float distanceTouziWeapon  = (Player.player.transform.position - Player.uziWeapon.transform.position).sqrMagnitude;
            Player.weaponCollector.CollectWeapon();

            yield return new WaitForSeconds(0.1f);

            Assert.That(distanceToshotgunWeapon < distanceTouziWeapon && Player.shotgunWeapon == null);
        }

        [UnityTest]
        public IEnumerator Test7_GettingOutOfRangeOfWeaponTest() {
            Player.ChangePlayerPosition(0, -15);
            Player.InstantiateShotgun(1f, -15);
            Player.InstantiateUzi(-0.5f, -15);
            
            yield return new WaitForSeconds(0.1f);

            Player.ChangePlayerPosition(1, -15);

            yield return new WaitForSeconds(0.1f);

            Player.weaponCollector.CollectWeapon();
            yield return new WaitForSeconds(0.1f);

            Assert.AreEqual(Player.weaponHolder.GetCurrentlyHoldedWeapon().runtimeAnimator.name, "ShotgunController");
            Assert.That(Player.shotgunWeapon == null);
            Assert.IsNotNull(Player.uziWeapon);
        }

        [TearDown]
        public void Clean() {
            Object.Destroy(Player.player);
            Object.Destroy(Player.camera);
            Player.ClearWeaponFromScreen();
        }
    }
}
