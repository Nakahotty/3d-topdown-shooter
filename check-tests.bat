@ECHO OFF
:CheckForFile
IF EXIST %1 GOTO FoundIt

ping -n 5 127.0.0.1 >NUL

GOTO CheckForFile


:FoundIt
findstr Failed %1
if %ERRORLEVEL% EQU 0 (exit /b 1) else (exit /b 0)