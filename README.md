# Zombie Royale 2D

![Main Menu](./Docs/main_menu.png "Main Menu")

</br>

![Gameplay](./Docs/gameplay.png "Gameplay")
## Final project for Clean code and Game Dev courses at FMI.

### This game is a top down shooter group project. You have to use a variety of weapons to survive in a forest and kill as many enemies as you can to achieve a high score. The game gets harder as you play. 

</br>

### You can enjoy the game from your browser right now - here is the latest game version - [PLAY NOW](https://nakahotty.gitlab.io/3d-topdown-shooter/master/)
### Or you can try out the game during *future* development - [CHECK OUT](https://nakahotty.gitlab.io/3d-topdown-shooter/develop/)

### Used technologies and packages
- C# programming language
- Unity Game Engine
- GitLab CI/CD + GitLab pages
- [StyleCopCLI](https://github.com/bbadjari/stylecopcli)
- [Cinemachine](https://unity.com/unity/features/editor/art-and-design/cinemachine)
- [A* Pathfinding Project](https://arongranberg.com/astar/)
- Hotline Miami Assets were used for developing alpha.
- Tilemap Assets from [OpenGameArt](https://opengameart.org)  

#### *Copyright All rights reserved for Nasko, Ivo, Dancho, Niki. The project is strictly licensed and no files or assets are allowed for use in any other projects outside Zombie Royale 2D.* 