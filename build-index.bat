@echo off
SET fileName=public/index.html 

REM Add top boilerplate html
echo|set /p="<!DOCTYPE html>" > %fileName% 
echo. >> %fileName%
echo|set /p="<html>" >> %fileName%
echo. >> %fileName%
echo|set /p="<body>" >> %fileName%
echo. >> %fileName%

REM go through all files and add the branch name + playtesting link
for /d %%d in (public/*.*) do (
	echo|set /p="   <a href='"  >> %fileName%
	echo|set /p=%%d/index.html >> %fileName%
	echo|set /p="' target='_blank'><h3>"  >> %fileName%
	echo|set /p=%%d>> %fileName%
	echo|set /p="</h3></a>"  >> %fileName%
	echo. >> %fileName%
	echo|set /p="<br>"  >> %fileName%
	echo. >> %fileName%
)

REM Add bottom boilerplate html
echo|set /p="</body>" >> %fileName%
echo. >> %fileName%
echo|set /p="</html>" >> %fileName%
echo. >> %fileName%