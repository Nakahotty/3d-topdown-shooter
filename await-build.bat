@ECHO OFF
:CheckForFile
IF EXIST %1 GOTO FoundIt
>nul find "Build Failed" log.txt 2>NUL && (
  echo Build Failed
  exit /B 1
)
ping -n 5 127.0.0.1 >NUL

GOTO CheckForFile

:FoundIt
echo Build Successful
exit /B