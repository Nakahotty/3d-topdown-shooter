@ECHO OFF


if not exist ./Assets/Scripts/ (
  echo Scripts folder does not exist!
  exit /b 0
)

stylecopCLI -r -cs ./Assets/Scripts/*
if %ERRORLEVEL% EQU 0 (
	@ECHO ON
	echo Stylecop PASSED.
	@ECHO OFF
) else (
	@ECHO ON
	echo Stylecop FAILED.
	@ECHO OFF
	exit /b 1
)